using NLog.Extensions.Logging;

namespace ChongShi.Mysql.Backup;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = Host.CreateApplicationBuilder(args);
        builder.Logging.ClearProviders();
        builder.Logging.AddNLog();
        builder.Services.AddWindowsService();
        builder.Services.AddHostedService<BackupWorker>();
        
        var host = builder.Build();
        host.Run();
    }
}