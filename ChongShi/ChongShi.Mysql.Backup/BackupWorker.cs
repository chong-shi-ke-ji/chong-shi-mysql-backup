using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace ChongShi.Mysql.Backup;

public class BackupWorker : BackgroundService
{
    private readonly ILogger<BackupWorker> _logger;
    private readonly string _baseDir;
    private readonly string _backupDir;
    private readonly string _host;
    private readonly string _port;
    private readonly string _dbName;
    private readonly string _user;
    private readonly string _pwd;

    public BackupWorker(ILogger<BackupWorker> logger, IConfiguration config)
    {
        _logger = logger;
        _baseDir = AppDomain.CurrentDomain.BaseDirectory;
        _host = config["DbHost"] ?? "localhost";
        _port = config["DbPort"] ?? "3306";
        _backupDir = config["BackupDir"] ?? Path.Combine(_baseDir, "Backup");
        _user = config["DbUser"] ?? "root";
        _pwd = config["BbPwd"] ?? "root@2024";
        _dbName = config["DbName"] ?? "DemoDb";
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
             //   await BackupDbTaskAsync();
               if (DateTime.Now.Hour.Equals(1) && DateTime.Now.Minute <= 10)
              //if (DateTime.Now.Second % 10 == 0)
                {
                    await BackupDbTaskAsync();
                    await RemoveOldDbBackupSync();   
                }
            // await Task.Delay(1000 * 60 * 30, stoppingToken);  
             await Task.Delay(1000 * 60 * 10, stoppingToken);
            }
        }
        catch (Exception e)
        {
            _logger.LogError(JsonConvert.SerializeObject(e));
        }
    }

    private async Task BackupDbTaskAsync()
    {
        await Task.Factory.StartNew(() =>
        {
            var dumpTool = string.Empty;

            // byd --result-file=/root/byd_192_168_2_144-2024_09_20_00_14_13-dump.sql --user=root --host=192.168.2.
            var arguments = string.Empty;

            var desFile = Path.Combine(_backupDir, $"{_dbName}_{DateTime.Now.ToString("yyyyMMdd")}.sql");
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                dumpTool = Path.Combine(_baseDir, "Mysql", "Windows", "bin", "mysqldump.exe");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                dumpTool = Path.Combine(_baseDir, "Mysql", "Linux", "bin", "mysqldump");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
            }
            else
            {
                throw new Exception("不支持此操作系统!");
            }

            arguments =
                $"{_dbName} --result-file={desFile} --user={_user} --password={_pwd} --host={_host} --port={_port}";
            using (var process = new Process())
            {
                process.StartInfo.FileName = dumpTool;
                process.StartInfo.Arguments = arguments;
                _logger.LogInformation(arguments);
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;

                // 启动进程
                process.Start();

                // 标准输出（
                string sOutPut = process.StandardOutput.ReadToEnd();
                if (!string.IsNullOrEmpty(sOutPut))
                {
                    _logger.LogInformation(sOutPut);
                }

                // 错误输出
                string error = process.StandardError.ReadToEnd();
                if (!string.IsNullOrEmpty(error) && error.Contains("Warning"))
                {
                    _logger.LogWarning(error);
                }

                if (string.IsNullOrEmpty(error))
                {
                    _logger.LogError(error);
                }

                process.WaitForExit();
                _logger.LogInformation($"{desFile}->备份成功!");
            }
        });
    }

    private async Task RemoveOldDbBackupSync()
    {
        await Task.Factory.StartNew(() =>
        {
            // 获取文件夹信息
            DirectoryInfo dirInfo = new DirectoryInfo(_backupDir);

            // 获取文件夹中的文件信息
            FileInfo[] files = dirInfo.GetFiles();

            // 遍历并打印文件名
            foreach (FileInfo file in files)
            {
                var delName = $"{_dbName}_{DateTime.Now.AddDays(-7).ToString("yyyyMMdd")}";
                if (File.Exists(file.FullName) && file.Name.Contains(delName))
                {
                    File.Delete(file.FullName);
                    _logger.LogInformation($"删除文件->{file.Name}");
                }
            }
        });
    }
}
